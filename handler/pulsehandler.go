package handler

import "net/http"

func PulseHandler(w http.ResponseWriter, _ *http.Request) {
	_, err := w.Write([]byte("server is up and running"))
	if err != nil {
		http.Error(w, http.StatusText(503), 503)
	}
}
