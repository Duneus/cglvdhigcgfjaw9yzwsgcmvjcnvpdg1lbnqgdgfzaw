package handler

import (
	weather "cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw"
	"cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw/service"
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

type APIKey string
type APIUrl string
type APIResponse struct {
	StatusCode int               `json:"statusCode"`
	Response   []weather.Weather `json:"response"`
	Error      string            `json:"error"`
}

type WeatherHandler struct {
	service *service.WeatherService
}

func WithService(service *service.WeatherService) func(h *WeatherHandler) {
	return func(h *WeatherHandler) {
		h.service = service
	}
}

func NewWeatherHandler(opts ...func(w *WeatherHandler)) *WeatherHandler {
	value := &WeatherHandler{}
	for _, opt := range opts {
		opt(value)
	}
	return value
}

func (h *WeatherHandler) Handler(w http.ResponseWriter, req *http.Request) {
	var locations []weather.Location
	cities := req.FormValue("cities")
	var err error
	var response APIResponse

	w.Header().Set("Content-Type", "application/json")

	if len(cities) == 0 {
		response.Error = "You need to provide cities to receive a response"
		response.Response = nil
		response.StatusCode = 400
	} else {
		v := strings.Split(cities, ",")

		for i := 0; i < len(v); i++ {
			locations = append(locations, weather.Location(v[i]))
		}

		result, err := h.service.GetAll(locations)

		if err != nil {
			log.Fatalln("horrible error", err)
		}

		response.Error = ""
		response.Response = result
		response.StatusCode = 200
	}

	if err = json.NewEncoder(w).Encode(response); err != nil {
		log.Fatalln("handled", err)
	}

}
