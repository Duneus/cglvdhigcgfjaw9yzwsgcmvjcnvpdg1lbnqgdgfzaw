FROM golang:1.13.5

WORKDIR /go/src/cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw

COPY . .

RUN go mod download

RUN go build -o main ./cmd

CMD ["./main"]
