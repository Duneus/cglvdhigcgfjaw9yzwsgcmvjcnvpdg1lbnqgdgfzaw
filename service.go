package weather

type Service interface {
	Get(l Location) (Weather, error)
	GetAll(l []Location) ([]Weather, error)
}
