package openWeather

import (
	weather "cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw"
	"encoding/json"
)

type coord struct {
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}

type weatherdata []struct {
	ID          int    `json:"id"`
	Main        string `json:"main"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
}

type main struct {
	Temp     float64 `json:"temp"`
	Pressure int     `json:"pressure"`
	Humidity int     `json:"humidity"`
	TempMin  float64 `json:"temp_min"`
	TempMax  float64 `json:"temp_max"`
}

type wind struct {
	Speed float64 `json:"speed"`
	Deg   float64 `json:"deg"`
}

type clouds struct {
	All int `json:"all"`
}

type sys struct {
	Type    int     `json:"type"`
	ID      int     `json:"id"`
	Message float64 `json:"message"`
	Country string  `json:"country"`
	Sunrise int     `json:"sunrise"`
	Sunset  int     `json:"sunset"`
}

type Response struct {
	Coord    coord           `json:"coord"`
	Weather  weatherdata     `json:"weather"`
	Base     string          `json:"base"`
	Main     main            `json:"main"`
	Wind     wind            `json:"wind"`
	Clouds   clouds          `json:"clouds"`
	Dt       int             `json:"dt"`
	Sys      sys             `json:"sys"`
	Timezone int             `json:"timezone"`
	ID       int             `json:"id"`
	Name     weather.Weather `json:"name"`
	Cod      float64         `json:"cod"`
}

func decode(b []byte) (weather.Weather, error) {
	var data Response
	err := json.Unmarshal(b, &data)
	if err != nil {
		return "", nil
	}

	return data.Name, nil
}

func (r Response) encode() ([]byte, error) {
	return json.Marshal(Response{
		Coord:    coord{},
		Weather:  nil,
		Base:     "",
		Main:     main{},
		Wind:     wind{},
		Clouds:   clouds{},
		Dt:       0,
		Sys:      sys{},
		Timezone: 0,
		ID:       0,
		Name:     "",
		Cod:      0,
	})
}
