package openWeather

import (
	weather "cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw"
	"io/ioutil"
	"net/http"
)

type WeatherProvider struct {
	apiKey string
	apiUrl string
}

func WithAPIKey(apiKey string) func(s *WeatherProvider) {
	return func(s *WeatherProvider) {
		s.apiKey = apiKey
	}
}

func WithAPIUrl(apiUrl string) func(s *WeatherProvider) {
	return func(s *WeatherProvider) {
		s.apiUrl = apiUrl
	}
}

func NewWeatherProvider(opts ...func(w *WeatherProvider)) *WeatherProvider {
	value := &WeatherProvider{
		apiUrl: "http://api.openweathermap.org/data/2.5/weather?q=",
	}

	for _, opt := range opts {
		opt(value)
	}
	return value
}

func (p *WeatherProvider) Get(l weather.Location) (weather.Weather, error) {
	result, err := http.Get(p.apiUrl + string(l) + "&APPID=" + p.apiKey)
	if err != nil {
		return "", err
	}
	val, err := ioutil.ReadAll(result.Body)
	if err != nil {
		return "", err
	}
	value, err := decode(val)
	if err != nil {
		return "", err
	}
	return value, nil
}
