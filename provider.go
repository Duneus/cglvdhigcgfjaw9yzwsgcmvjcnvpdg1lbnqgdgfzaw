package weather

type Provider interface {
	Get(l Location) (Weather, error)
}
