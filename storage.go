package weather

type Location string
type Weather string

type Storage interface {
	Store(l Location, v Weather) error
	Get(l Location) (Weather, error)
}
