package redis

import (
	weather "cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw"
	"github.com/go-redis/redis/v7"
	"sync"
	"time"
)

type WeatherStorage struct {
	options *redis.Options
	memory  *redis.Client
	mut     sync.RWMutex
	timeout time.Duration
}

func WithOptions(addr string, password string, db int) func(s *WeatherStorage) {
	return func(s *WeatherStorage) {
		s.options = &redis.Options{
			Addr:     addr,
			Password: password,
			DB:       db,
		}
	}
}

func WithTimeout(timeout time.Duration) func(s *WeatherStorage) {
	return func(s *WeatherStorage) {
		s.timeout = timeout
	}
}

func NewWeatherStorage(opts ...func(storage *WeatherStorage)) *WeatherStorage {
	value := &WeatherStorage{
		memory:  nil,
		timeout: 10 * time.Minute,
		options: &redis.Options{},
	}

	for _, opt := range opts {
		opt(value)
	}

	value.memory = redis.NewClient(value.options)

	return value
}

func (w *WeatherStorage) Store(l weather.Location, v weather.Weather) error {
	w.mut.Lock()
	defer w.mut.Unlock()
	err := w.memory.Set(string(l), v, w.timeout).Err()
	if err != nil {
		panic(err)
	}
	return nil
}

func (w *WeatherStorage) Get(l weather.Location) (weather.Weather, error) {
	w.mut.Lock()
	defer w.mut.Unlock()
	result, err := w.memory.Get(string(l)).Result()
	if err == redis.Nil {
		return "", err
	}
	if err != nil {
		panic(err)
	}

	return weather.Weather(result), nil
}
