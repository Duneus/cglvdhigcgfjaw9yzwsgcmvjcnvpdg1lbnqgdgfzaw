package main

import (
	"cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw/handler"
	"cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw/openWeather"
	"cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw/redis"
	"cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw/service"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/kelseyhightower/envconfig"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Config struct {
	RedisUrl      string `envconfig:"REDIS_URL" default:"redis:6379"`
	AppPort       string `envconfig:"APP_PORT" default:":8000"`
	WeatherApiKey string `envconfig:"WEATHER_API_KEY"`
	RedisExpire   string `envconfig:"REDIS_EXPIRE" default:"10"` // minutes
	Timeout       string `envconfig:"TIMEOUT" default:"60"`      //seconds
}

func main() {
	var conf Config
	err := envconfig.Process("", &conf)
	if err != nil {
		log.Fatal(err.Error())
	}

	redisExpire, err := strconv.ParseInt(conf.RedisExpire, 10, 32)
	if err != nil {
		log.Fatal(err.Error())
	}

	storage := redis.NewWeatherStorage(
		redis.WithOptions(
			conf.RedisUrl,
			"",
			0,
		),
		redis.WithTimeout(time.Duration(redisExpire)*time.Minute),
	)
	weatherProvider := openWeather.NewWeatherProvider(openWeather.WithAPIKey(conf.WeatherApiKey))
	weatherService := service.NewWeatherService(
		service.WithProvider(weatherProvider),
		service.WithStorage(storage),
	)
	weatherHandler := handler.NewWeatherHandler(handler.WithService(weatherService))

	timeout, err := strconv.ParseInt(conf.Timeout, 10, 32)
	if err != nil {
		log.Fatal(err.Error())
	}

	r := chi.NewRouter()

	r.Use(
		middleware.Logger,
		middleware.Recoverer,
		middleware.Timeout(time.Duration(timeout)*time.Second),
	)

	r.Get("/", weatherHandler.Handler)
	r.Get("/pulse", handler.PulseHandler)

	fmt.Println("Starting app under port", conf.AppPort)
	if err := http.ListenAndServe(conf.AppPort, r); err != nil {
		log.Fatalln("massive error", err)
	}
}
