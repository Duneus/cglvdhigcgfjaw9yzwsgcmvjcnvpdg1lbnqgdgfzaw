module cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw

go 1.13

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-redis/redis/v7 v7.0.0-beta.5
	github.com/golang/mock v1.3.1
	github.com/kelseyhightower/envconfig v1.4.0 // indirect
)
