do odpalenia
```
docker build -t appka
docker run -d -p 8000:8000 appka
```

a potem w przeglądarce 
```
http://localhost:8000/?cities=london
```

ewentualnie `docker run main.go` też zadziała, ustawiwszy odpowiednie envy:
`APP_PORT` oraz `WEATHER_API_KEY`. To drugie jest w Dockerfile'u.