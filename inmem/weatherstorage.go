package inmem

import (
	weather "cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw"
	"errors"
	"sync"
	"time"
)

type weatherEntry struct {
	weather weather.Weather
	timeout time.Time
}
type WeatherStorage struct {
	memory  map[weather.Location]weatherEntry
	mut     sync.RWMutex
	timeout time.Duration
}

func WithTimeout(timeout time.Duration) func(s *WeatherStorage) {
	return func(s *WeatherStorage) {
		s.timeout = timeout
	}
}

func NewWeatherStorage(opts ...func(storage *WeatherStorage)) *WeatherStorage {
	value := &WeatherStorage{
		memory:  make(map[weather.Location]weatherEntry),
		timeout: 10 * time.Minute,
	}
	for _, opt := range opts {
		opt(value)
	}
	return value
}

func (w *WeatherStorage) Store(l weather.Location, v weather.Weather) error {
	w.mut.Lock()
	defer w.mut.Unlock()
	w.memory[l] = weatherEntry{
		v,
		time.Now(),
	}
	return nil
}

func (w *WeatherStorage) Get(l weather.Location) (weather.Weather, error) {
	w.mut.Lock()
	defer w.mut.Unlock()
	result, ok := w.memory[l]
	if !ok {
		return "", errors.New("result doesnt exist")
	}

	if result.timeout.Add(w.timeout).After(time.Now()) {
		return "", errors.New("result outdated")
	}

	return result.weather, nil
}
