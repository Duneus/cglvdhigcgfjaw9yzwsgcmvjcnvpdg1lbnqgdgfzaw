package service

import (
	weather "cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw"
	"cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw/openWeather"
	"cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw/redis"
	"fmt"
	"log"
)

type WeatherService struct {
	provider weather.Provider
	storage  weather.Storage
}

func WithProvider(provider *openWeather.WeatherProvider) func(s *WeatherService) {
	return func(s *WeatherService) {
		s.provider = provider
	}
}

func WithStorage(storage *redis.WeatherStorage) func(s *WeatherService) {
	return func(s *WeatherService) {
		s.storage = storage
	}
}

func NewWeatherService(opts ...func(service *WeatherService)) *WeatherService {
	value := &WeatherService{}
	for _, opt := range opts {
		opt(value)
	}
	return value
}

func (s *WeatherService) Get(l weather.Location) (weather.Weather, error) {
	value, err := s.storage.Get(l)
	if err != nil {
		value, err := s.provider.Get(l)
		if err != nil {
			fmt.Println("error")
			return "", err
		}
		return value, nil
	}
	return value, nil
}

func (s *WeatherService) GetAll(l []weather.Location) ([]weather.Weather, error) {
	var results []weather.Weather
	c := make(chan weather.Weather)

	for _, location := range l {
		go func(l weather.Location) {
			var result weather.Weather
			result, err := s.Get(l)

			if err != nil {
				log.Fatalln("cannot get proper data", err)
			}

			c <- result
		}(location)
	}

	for i := 0; i < len(l); i++ {
		value := <-c
		results = append(results, value)
	}

	return results, nil
}
