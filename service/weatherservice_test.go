package service

import (
	"cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw"
	mock_weather "cGlvdHIgcGFjaW9yZWsgcmVjcnVpdG1lbnQgdGFzaw/mocks"
	"github.com/golang/mock/gomock"
	"testing"
)

func TestNewWeatherService(t *testing.T) {
	ctrl := gomock.NewController(t)

	defer ctrl.Finish()

	m := mock_weather.NewMockService(ctrl)

	m.EXPECT().Get(gomock.Eq(weather.Location("london"))).Return(weather.Weather("london"), nil)
	m.EXPECT().GetAll(gomock.Eq([]weather.Location{"london", "warsaw"})).Return([]weather.Weather{"london", "warsaw"}, nil)

	_, err := m.Get("london")
	if err != nil {
		t.Error(err)
	}

	_, err = m.GetAll([]weather.Location{"london", "warsaw"})
	if err != nil {
		t.Error(err)
	}

}


